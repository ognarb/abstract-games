import QtQuick 2.7 

Rectangle
{
	id: block
	property int type
	property bool dying: false


	color: type == 0 ? "#F44336" : type == 1 ? "#4CAF50" : type == 2 ? "#2196F3" : "#2196F3" 
	visible: !dying

	Behavior on y 
	{
		NumberAnimation {
			duration: 100;
		}
	}

	Behavior on x
	{
		NumberAnimation {
			duration: 100;
		}
	}
}
