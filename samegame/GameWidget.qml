import QtQuick 2.7
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	property int score: 0
	property int n: gamewidget.difficulty == "easy" ? 9 : gamewidget.difficulty == "medium" ? 12 : 15
	property int numcolors: gamewidget.difficulty != "hard" ? 3 : 4

	onGameRestarted:
	{
		Game.Restart();
	}

	onClicked:
	{
		Game.Click(x, y);
	}
}
