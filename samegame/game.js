/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*
	Small uillity functions.
*/

function ToIndex(x, y)
{
	return [Math.floor(x / (gamewidget.width / gamewidget.n)), Math.floor(y / (gamewidget.height / gamewidget.n))];
}

function GetPos(i) {
        var r, c;
        var k = 1;

        for (var j = 0; j < gamewidget.n*gamewidget.n; j++)
        {
                if (j % gamewidget.n == 0 && j != 0) k++;
                if (i < k * gamewidget.n)
                {
                        c = k - 1;
                        r = i - ((k - 1) * gamewidget.n);
                        break;
                }
        }

        return [r, c];
}

function GetInvPos(r, c) {
        if (r >= gamewidget.n || r < 0 || c >= gamewidget.n || c < 0) return -1;

        return r + c * gamewidget.n
}

/*
	Core game functions.
*/

var board;
var component;

function Restart() {
	gamewidget.score = 0;

	if (board)
	{
		for (var i = 0; i < board.length; i++)
		{
			if (board[i]) board[i].destroy();
		}
	}

	board = new Array(gamewidget.n*gamewidget.n);
	component = Qt.createComponent("block.qml");

	for (var i = 0; i < gamewidget.n*gamewidget.n; i++) {

		board[i] = component.createObject(gamewidget, {"x": (GetPos(i)[0] * (gamewidget.width / gamewidget.n)), "y": (GetPos(i)[1] * (gamewidget.height / gamewidget.n)), 
		"width": (gamewidget.width / gamewidget.n), "height": (gamewidget.height / gamewidget.n), "type": Math.floor(Math.random() * gamewidget.numcolors)});
		
	}
}

var fillFound;
var floodBoard;

function Click(x, y) {
	fill(ToIndex(x, y)[0], ToIndex(x, y)[1], -1);
	if (fillFound != 0)
	{
		gamewidget.score += (fillFound - 1) * (fillFound - 1);
		shuffle();

		if (!check(0, gamewidget.n - 1, -1))
		{
			var none = true;
			for (var i = 0; i < gamewidget.n*gamewidget.n; i++)
			{
				if (board[i])
				{
					none = false;
					break;
				}
			}

			/*if (none)
			{
				if (msg.easy)
				{
					gamewidget.score += 500;
				}
				else if (msg.medium)
				{
					gamewidget.score += 1000;
				}
				else
				{
					gamewidget.score += 5000;
				}
			}*/

			if (none)
			{
				score += gamewidget.difficulty == "easy" ? 100 : gamewidget.difficulty == "medium" ? 1000 : 5000
			}

			gamewidget.endGame("You Got " + gamewidget.score + " Points");
		}
	}
}

function shuffle()
{
	for (var x = 0; x < gamewidget.n; x++) {
		var fallDist = 0;
		for (var y = gamewidget.n - 1; y >= 0; y--)
		{
			if (board[GetInvPos(x, y)] == null)
			{
				fallDist++;
			}
			else
			{
				if (fallDist > 0)
				{
					var obj = board[GetInvPos(x, y)];
					obj.y = (y + fallDist) * (gamewidget.height / gamewidget.n);
					board[GetInvPos(x, y + fallDist)] = obj;
					board[GetInvPos(x, y)] = null;
				}
			}
		}
	}

	var fallDist = 0;

	for (var x = 0; x < gamewidget.n; x++)
	{
		if (board[GetInvPos(x, gamewidget.n - 1)] == null) {
			fallDist++;
		}
		else {
			if (fallDist > 0) {
				for (var y = 0; y < gamewidget.n; y++)
				{
					var obj = board[GetInvPos(x, y)];
					if (obj == null)
						continue;
					obj.x = (x - fallDist) * (gamewidget.width / gamewidget.n);
					board[GetInvPos(x - fallDist,y)] = obj;
					board[GetInvPos(x, y)] = null;
				}
			}
		}
	}
}

function fill(x, y, tp) {
	var index = GetInvPos(x, y);

	if (board[index] == null)
		return;
	if (x >= board.n || x < 0 || y >= board.n || y < 0) return false;

	var first = false;
	if (tp == -1) {
		first = true;
		tp = board[index].type;
		fillFound = 0;
		floodBoard = new Array(gamewidget.n*gamewidget.n);
	}

	if (floodBoard[index] == 1 || (!first && tp != board[index].type))
		return;

	floodBoard[index] = 1;
	fill(x + 1, y, tp);
	fill(x - 1, y, tp);
	fill(x, y + 1, tp);
	fill(x, y - 1, tp);

	if (first == true && fillFound == 0)
		return;

	board[index].dying = true
	board[index] = null;
	fillFound += 1;
}

function check(x, y, tp)
{
	var index = GetInvPos(x, y);
	if (board[index] == null)
	{
		return false;
	}
	if (x >= board.n || x < 0 || y >= board.n || y < 0) return false;

	var type = board[index].type;
	if (tp == type)
		return true;

	return check(x + 1, y, type) || check(x, y - 1, board[GetInvPos(x, y)].type);

}
