/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.7
import QtQuick.Controls 2.1
import "game.js" as Game


GameController {
	gameType: "ArcadeGame"
	name: "SameGame"
}
