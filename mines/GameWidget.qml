/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	onGameRestarted:
	{
		Game.Restart();
	}

	Grid {
		id: board
		anchors.fill: parent
		rows: board.n
		columns: board.n

		property int mines: gamewidget.difficulty == "easy" ? 5 : gamewidget.difficulty == "medium" ? 10 : 30
		property int n: gamewidget.difficulty == "easy" ? 10 : gamewidget.difficulty == "medium" ? 12 : 15
		property bool pause: false

		Repeater {
			model: board.n*board.n

			Rectangle {
				id: rect
				width: gamewidget.width/board.n
				height: gamewidget.height/board.n
				color: "transparent"
				z: 0

				border {
					color: "black"
					width: 1
				}

				Label {
					font.family: "Ubuntu"
					font.pointSize: 15
					font.bold: true
					id: text
					color: text.color
					property bool bomb: false
                                        property bool nb: false
                                        property bool fb: false
                                        property bool nfb: false
                                        property string oldtext: ""
					property int time
					anchors.centerIn: parent
					visible: false

						ParallelAnimation {
							running: text.bomb
	
						NumberAnimation {
							target: text 
							property: "visible"
							from: 0
							to: 1
							duration: text.time
						}

						ColorAnimation {
							target: rect 
							property: "color"
							from: "gray"
							to: "red"
							duration: text.time 
						}
					}


					ColorAnimation {
						running: text.nb
						target: rect 
						properties: "color"
						duration: 1500
						from: "gray"
						to: "green"
					}

					ColorAnimation {
						running: text.fb
						target: rect
						properties: "color"
						duration: 1500
						from: "gray"
						to: "blue"
					}

					ColorAnimation {
						running: text.nfb
						target: rect
						properties: "color"
						duration: 1500
						from: "blue"
						to: "gray"
					}
				}

				ToolButton {
					id: toolbutton
					anchors.fill: parent

					onReleased: {
						console.log("2: It Works!");
					}

					MouseArea {
						id: mousearea
                                                anchors.fill: parent
						acceptedButtons: Qt.LeftButton | Qt.RightButton
						propagateComposedEvents: true

						onClicked: {
							mouse.accepted = false;
                                                        if (mouse.button == undefined || mouse.button == Qt.RightButton)
                                                        {
                                                                Game.Click(index, true);
                                                        }
                                                        else
                                                        {
                                                                Game.Click(index, false);
                                                        }

                                                        }
						}
                                }
			}
		}
	}
}
