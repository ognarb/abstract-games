import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	onGameRestarted: {
		Game.Restart();
	}

		Grid {
			id: board
			rows: 8
			columns: 8

			property string turn: "#ffffff"
			property bool check: false
			property bool moving2black: false
			property bool moving2white: false
			property bool first: false
			property var pawns_white: [true, true, true, true, true, true, true, true]
			property var pawns_black: [true, true, true, true, true, true, true, true]
			property bool pause: false
			
			property bool is_greyed: spinner.running
			property bool is_moving: false
			//property bool pause: board.pause

			SequentialAnimation
			{
				running: board.moving2black

				ScriptAction
				{
					script:
					{
						;//msg.visible = false;
					}
				}

				RotationAnimation
				{
					target: board
					properties: "rotation"
					duration: 750
					from: 0
					to: 180 
				}

				ScriptAction
				{
					script:
					{
						//msg.visible = true;
						board.moving2black = false;
					}
				}
			}

			SequentialAnimation
			{
				running: board.moving2white

				ScriptAction
				{
					script:
					{
						;//msg.visible = false;
					}
				}

				RotationAnimation
				{
					target: board
					properties: "rotation"
					duration: 750
					from: 180
					to: 0
				}

				ScriptAction
				{
					script:
					{
						//msg.visible = true;
						board.moving2white = false;
					}
				}
			}

			Repeater {
				model: 64

				Rectangle {
					width: gamewidget.width/8
					height: gamewidget.height/8
					id: rect

					property bool white: false 
					property bool selected: false
					property bool nx: false
					property bool fx: false
					property bool aix: false
					property string org_color: "";

					color: white ? selected ? Qt.darker("lightgray", 1.05) : "lightgray" : selected ? 
						Qt.darker("darkgray", 1.05) : "darkgray"

					Text {
						id: text
						anchors.centerIn: parent
						opacity: 0
					}

					Image {
						id: image 
						anchors.centerIn: parent
						source: Game.image(text.text, text.color)
						smooth: true
						sourceSize.width: 45 
						sourceSize.height: 45
						rotation: board.rotation
					}

					SequentialAnimation
					{
						running: rect.aix


						ScriptAction
						{ script: {board.is_moving = true;board.is_greyed = true; pulser.visible = true;}}

						SequentialAnimation
                                                {       
                                                        loops: 8
                                                        
                                                        NumberAnimation
                                                        {
								target: pulser
								properties: "scale"
								from: 0.5
								to: 1
								duration: 500
								easing.type: Easing.InQuart
                                                        }

                                                        NumberAnimation
                                                        {
                                                                target: pulser; properties: "scale"; from: 1; to: 0.5; duration: 500; easing.type: Easing.OutQuart;
                                                        }

                                                }

						ScriptAction
						{ script: {board.is_moving = false;board.is_greyed = false; pulser.visible = false;rect.aix=false}}
					}

					//border {
					//	width: 5 
					//	color: rect.color == "#ff0000" ? "#ffffff" : "#00000000"
					//}

					ToolButton {
						anchors.fill: parent
						checkable: true
						onClicked: {
							if (board.is_greyed) return;

							if (board.children[index].children[0].text != "" && board.children[index].children[0].color == board.turn) {
								if (board.children[index].selected) {
									board.children[index].border.color = "#00000000";
									board.children[index].selected = false;
								} else {
									console.log("foo");
									for (var i = 0; i < 64; i++)
									{
										if (board.children[i].selected && 
											(board.children[i].children[0].color != board.turn || 
											board.children[i].children[0].text == "")) {
											Game.MakeMove(i, index);
											return;
										}
										else if (board.children[i].selected)
										{
											board.children[i].selected = false;
											board.children[i].border.color = "#00000000";
										}
									}
									//board.children[index].border.color = "black";
									//board.children[index].border.width = "2";
									board.children[index].selected = true;
								}
							} else {
								for (var i = 0; i < 64; i++) 
									if (board.children[i].selected)
										Game.MakeMove(i, index);
							}
						}
					}

					Rectangle
                                        {
                                                width: 2 * rect.width;
                                                height: 2 * rect.height
                                                radius: 100
                                                smooth: true
                                                visible: false
                                                id: pulser
						x: board.children[index].x - 0.5*board.children[index].width
						y: board.children[index].y - 0.5*board.children[index].height
                                                color: "transparent";

                                                border { color: "#F44336"; width: 2;  }
						parent: board 
                                        }
				}
			}
		}

		Rectangle
		{
			visible: spinner.running
			anchors.fill: parent

			color: "#555555"
			opacity: 0.5
		}

		BusyIndicator
		{
			id: spinner
			running: false
			anchors.centerIn: parent
			Material.accent: Material.Green
		}

		Text
		{
			visible: spinner.running
			font.family: "Ubuntu"
			anchors.top: spinner.bottom
			anchors.topMargin: 10
			anchors.horizontalCenter: spinner.horizontalCenter
			text: "Spectrum is thinking..."
			font.pointSize: 12
			color: "white"
		}
}
