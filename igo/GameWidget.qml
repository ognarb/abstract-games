import QtQuick 2.7
import "game.js" as Game

SimpleGameWidget {
	id: gamewidget

	onGameRestarted: {
		Game.Restart();
	}
	
	    Grid {
		    anchors.fill: parent
			property int n: 19 

			id: board
			columns: n 
			rows: n

			property string player
			property bool pause: false


			Repeater {
				model: board.n*board.n 

				Rectangle {
					width: gamewidget.width/board.n
					height: gamewidget.height/board.n
					color: "#B0BEC5"
					id: square

					MouseArea {
						id: mousearea

						onClicked: {
							Game.Click(index);
						}

						anchors.centerIn: parent
						anchors.fill: parent
					}

					Image {
						id: img
						property string color: ""
						//radius: 50
						smooth: true
						source: color == "black" ? "images/black.png" : "images/white.png"
						anchors.centerIn: parent
						width: (gamewidget.width/board.n)
						height: (gamewidget.height/board.n)
						visible: false
						z: 1
					}

					Rectangle {
						anchors.centerIn: parent
                                                width: 1
						height: parent.height
						color: "black"
					}

					Rectangle {
						anchors.centerIn: parent
						color: "black"
                                                height: 1
                                                width: parent.width 
					}

					Rectangle
					{
						anchors.top: parent.top
						visible: index < board.n ? true : false
						color: "#B0BEC5"
				                width: gamewidget.width
				                height: (0.5 * (gamewidget.height / board.n)) - 1
					}

					Rectangle
					{
						anchors.bottom: parent.bottom
                                                visible: index >= ((board.n * board.n) - board.n) ? true : false
                                                color: "#B0BEC5"
                                                width: gamewidget.width
                                                height: (0.5 * (gamewidget.height / board.n)) - 1
					}

					Rectangle
					{
						anchors.left: parent.left
                                                visible: index % board.n == 0 ? true : false
                                                color: "#B0BEC5"
                                                height: gamewidget.height
                                                width: (0.5 * (gamewidget.width / board.n)) - 1
					}

					Rectangle
                                        {
                                                anchors.right: parent.right
                                                visible: (index - board.n - 18) % board.n == 0 ? true : false
                                                color: "#B0BEC5"
                                                height: gamewidget.height
                                                width: (0.5 * (gamewidget.width / board.n)) - 1
                                        }
				}
			}
		}
	}
