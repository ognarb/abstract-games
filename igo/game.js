/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

function PlayAI()
{
	var index = 0;

	while (true)
	{
		index = Math.floor(Math.random() * board.n * board.n);

		var visible = board.children[index].children[1].visible;
		var color = board.children[index].children[1].color.toString();

		board.children[index].children[1].visible = true;
        	board.children[index].children[1].color = board.player;

		if (visible)
		{
			board.children[index].children[1].visible = visible;
			board.children[index].children[1].color = color;
			continue;
		}

		if (Check(index))
		{
			board.children[index].children[1].visible = visible;
                        board.children[index].children[1].color = color;
                        continue;
		}

		break;
	}
}

function Restart()
{
	for (var i = 0; i < board.n*board.n; i++)
	{
		board.children[i].children[1].visible = false;
	}

	board.children[board.n * 3 + 3].children[1].visible = true;
	board.children[board.n * 3 + 3].children[1].color = "black";

	board.children[board.n * 3 + (board.n - 4)].children[1].visible = true;
        board.children[board.n * 3 + (board.n - 4)].children[1].color = "black";

	board.children[(board.n*board.n) - board.n * 4 + 3].children[1].visible = true;
        board.children[(board.n*board.n) - board.n * 4 + 3].children[1].color = "black";

        board.children[(board.n*board.n) - board.n * 4 + (board.n - 4)].children[1].visible = true;
        board.children[(board.n*board.n) - board.n * 4 + (board.n - 4)].children[1].color = "black";

	board.player = "white";
}

function Click(index)
{
	if (board.children[index].children[1].visible) return;

	board.children[index].children[1].visible = true;
	board.children[index].children[1].color = board.player;

	Check(index);

	if (gamewidget.players != "two")
	{
		PlayAI();
	}
}

function Check(index)
{
	var exit = 0;
	var all = true;
	var numwhite = 0, numblack = 0;

	for (var i = 0; i < board.n*board.n; i++)
	{
		if (board.children[i].children[1].visible)
		{
			var a = true;
			var c = board.children[i].children[1].color; 

			if (i - 1 >= 0 && (i % board.n))
			{
				if (!board.children[i - 1].children[1].visible || board.children[i - 1].children[1].color == c)
				{
					a = false;
				}
			}

			if (i + 1 < board.n*board.n)
                        {
                                if (!board.children[i + 1].children[1].visible || board.children[i + 1].children[1].color == c)
                                {
                                        a = false;
                                }
                        }

			if (i - board.n >= 0)
                        {
                                if (!board.children[i - board.n].children[1].visible || board.children[i - board.n].children[1].color == c)
                                {
                                        a = false;
                                }
                        }

			if (i + board.n < board.n*board.n)
                        {
                                if (!board.children[i + board.n].children[1].visible || board.children[i + board.n].children[1].color == c)
                                {
                                        a = false;
                                }
                        }

			if (a)
			{
				board.children[i].children[1].visible = false;
				if (i == index) exit += 1;
			}
		}
	}

	for (var i = 0; i < board.n*board.n; i++)
	{
		if (!board.children[i].children[1].visible) all = false;
		else
		{
			if (board.children[i].children[1].color == "white") numwhite++;
			else numblack++;
		}
	}

	if (all)
	{
		if (numwhite > numblack)
		{
			gamewidget.endGame("White won");
		}
		else
		{
			if (gamewidget.players != "two") gamewidget.endGame("Spectrum won"); 
			else gamewidget.endGame("Black won");
		}

		return;

	}

	if (board.player == "white") board.player = "black";
        else board.player = "white";

	return exit;
}
