/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*
	Small uillity functions.
*/

/*
	Core game functions.
*/

.import AbstractGames.Logic 1.0 as Logic

var inc, nspe;

function gotoNextLevel()
{
	jetplane.x = 10;
        jetplane.y = 10;
        gamewidget.lifes = gamewidget.difficulty == "hard" ? 3 : 5;
        gamewidget.speed = gamewidget.level == 0 ? 5 : gamewidget.level == 1 ? 5.5 : 6;
        gamewidget.maxheight = gamewidget.difficulty == "easy" ? 100 : gamewidget.difficulty == "medium" ? 125 : 150;
        timer.running = true;

	for (var i = 0; i < 10; i++)
        {
                buildings.children[i].height = Math.ceil(Math.random() * gamewidget.maxheight) + 100;
        }
	inc = false;
	nspe = gamewidget.speed;
}

function Restart()
{
	gamewidget.score = 0;
	gamewidget.level = 0;
	gotoNextLevel();
}

function burndown(i)
{
	buildings.children[i].height -= 25;

	if (buildings.children[i].height < 40) buildings.children[i].height = 0;
}

function check()
{
	for (var i = 0; i < 10; i++)
        {
                if (Logic.Collisions.isColliding(jetplane, buildings.children[i]) || jetplane.y < 0)
                {
                        gamewidget.lifes -= 1;

			if (jetplane.y > 0)
			{
				burndown(i);

				console.log(buildings.children[i].height);
				if (buildings.children[i].height == 0)
				{
					console.log("X");
					moveplane();
					return;
				}
			}

                        if (gamewidget.lifes > 0)
                        {
                                timer.running = false;
                                if (gamewidget.lifes != 1)
                                        gamewidget.updateStatus("You have " + gamewidget.lifes + " lives left");
                                else
                                        gamewidget.updateStatus("You have 1 life left");
                        }
                        else
                        {
                                timer.running = false;
                                timer.stop();
                                gamewidget.updateStatus("You lose.");
                                gotoNextLevel();
                                return;
                        }


                        //burndown(i);
                        jetplane.x = 10;
                        jetplane.y = 10;
                        timer.running = true;
                }
        }
}

function moveplane()
{
	var allbuildings = true;
	for (var i = 0; i < 10; i++)
	{
		if (buildings.children[i].height > 0)
		{
			allbuildings = false;
		}
	}

	if (allbuildings)
	{
		timer.running = false;
		timer.stop();
		if (gamewidget.level == gamewidget.maxlevels)
                                {
                                        gamewidget.endGame("You got " + gamewidget.score + " points");
                                        return;
                                }
                                else
                                {
                                        gamewidget.level++;
                                        gamewidget.updateStatus("Onto level " + (gamewidget.level + 1));
                                        gotoNextLevel();
                                }
		return;
	}

	/*
	if (c1 == undefined && c2 == undefined)
                        {
                                if (Logic.Collisions.isColliding(jetplane, c1) || Logic.Collisions.isColliding(jetplane, c2))
                                {
                                        if (!inc)
                                        {
                                                console.log("OOP");
                                                inc = true;
                                                gamewidget.speed = 2;
                                        }
                                }
                                else
                                {
                                        console.log("OOP@");
                                        inc = false;
                                        gamewidget.speed = nspe;
                                }
                        }*/

	jetplane.x += gamewidget.speed;

	if (jetplane.x > gamewidget.width - 10)
	{
		jetplane.x = 10;
		jetplane.y += 50;
	}

	check();
}
