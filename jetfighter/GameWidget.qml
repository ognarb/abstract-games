import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import AbstractGames.Logic 1.0 as Logic
import "game.js" as Game

SimpleGameWidget
{
	id: gamewidget

	property bool lock: false
	property var pause: false
	property int maxheight
	property double speed: 10
	property int lifes
	property int score
	property bool wahIn: false
	property int maxlevels: 2
	property int level: 0

	onGameRestarted:
	{
		Game.Restart();
	}

	onGamePaused:
	{
		timer.running = false;
	}

	onGameEnded:
	{
		timer.running = false;
	}

	onGameStarted:
	{
		timer.running = true;
	}

	onClicked:
	{
		if (thingie.visible) return;

		if (gamewidget.lock) return;

		gamewidget.lock = true;

		var component = Qt.createComponent("bomb.qml")
		var bomb = component.createObject(gamewidget, 
			{"x": jetplane.x, "y": jetplane.y, "width": gamewidget.width / 30, "height": gamewidget.height / 30});

		var i = 0;
		var step = 2;
		var stop = false;

		var its = 0;
		var nspe = (gamewidget.speed == 5 ? 6 : 7);
		var spe = nspe;
		var inc = false;

		while (true)
		{
			bomb.x -= step;
			bomb.y += spe;
			its++;
                        gamewidget.update();

			for (i = 0; i < 10; i++)
			{
				//if ((bomb.y > (gamewidget.height - buildings.children[i].height)) &&
				//	Math.abs(bomb.x - buildings.children[i].x) < 25)
				if (Logic.Collisions.isColliding(bomb, buildings.children[i]))
				{
					stop = true;
					break;
				}
			}

			/*if (c1 && c2)
			{
				if (Logic.Collisions.isColliding(bomb, c1) || Logic.Collisions.isColliding(bomb, c2))
				{
					if (!inc)
					{
						console.log("OOP");
						inc = true;
						spe = 2;
					}
				}
				else
				{
					console.log("OOP@");
					inc = false;
					spe = nspe;
				}
			}*/

			if (bomb.x < 0 || bomb.y > 600)
			{
				bomb.destroy();
				gamewidget.lock = false;
				return;
			}

			if (step > 0) step -= 0.05;
			if (stop) break;
		}

		bomb.destroy();
		Game.burndown(i);
		gamewidget.score += 5;
		gamewidget.lock = false;
	}

	Rectangle
	{
		id: jetplane
		x: 10
		y: 10
		height: 22.7
		width: 50
		color: "transparent"


		Image {
			anchors.fill: parent
	                source: "images/plane.svg"
		}
	}

	Item
	{
		id: buildings
		anchors.fill: parent

		Repeater
		{
			model: 10

			Rectangle
			{
				id: rect
				anchors.bottom: parent.bottom
				width: gamewidget.width / 13
				height: Math.ceil(Math.random() * gamewidget.maxheight) + 100

				x: ((index * rect.width) * 1.2) + 30

				Behavior on height {
                                                NumberAnimation
                                        {
                                                duration: 250
					}
				}

				color: "transparent"

				Image {
					anchors.fill: parent
					source: "images/mountain.svg"
					smooth: true
				}
			}
		}
	}

	/*Rectangle {
		id: c1
		z: 10000
		color: Qt.rgba(1, 1, 1, 0.9)
		x: 10
		y: 50
		radius: 10

		width: 300
		height: 100

		Timer {
			interval: 1
			repeat: true
			running: timer.running

			onTriggered: {
				parent.x += 0.1;

				if (parent.x > 300) parent.x = 0;
			}
		}
	}

	Rectangle {
		id: c2
                z: 10000
                color: Qt.rgba(1, 1, 1, 0.9)
                x: 10
                y: 210
                radius: 10

                width: 300
		height: 100

		 Timer {
                        interval: 1
                        repeat: true
                        running: timer.running

			onTriggered: {
                                parent.x += 0.2;

                                if (parent.x > 300) parent.x = 0;
                        }
                }
	}*/

	Timer
	{
		id: timer
		repeat: true
		interval: 10

		onTriggered:
		{
			if (Qt.application.state == Qt.ApplicationInactive || !gamewidget.containsMouse)
			{
				thingie.visible = true;
			}
			else
			{
				thingie.visible = false;
				Game.moveplane();
			}
		}
	}

	Rectangle
	{
		visible: false;
		id: thingie
		color: "#555555"
                        opacity: 0.5
		anchors.fill: parent

		Text
		{
			anchors.centerIn: parent
			text: "It looks like your window or mouse cursor isn't in focus any more."
			font.family: "Ubuntu"
			font.pointSize: 12
		}
	}
}

