import QtQuick 2.7
import QtGraphicalEffects 1.7

Item {
	Image
	{
		id: bomb
		smooth: true
		visible: false
		source: "images/bomb.svg"
	}

	DirectionalBlur {
		anchors.fill: parent
		source: bomb
		angle: 0
		length: 32
	}
}
