/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <QApplication>

#include "main.h"
#include "../libabstractgames/widgets/widgets.h"
#include "../libabstractgames/logic/logic.h"

int main(int argc, char* argv[])
{
	RegisterWidgets();
	RegisterLogic();

        QApplication* app = new QApplication(argc, argv);
	LaunchJetFighter();

        return app->exec();
}
