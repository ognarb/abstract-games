import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1
import "game.js" as Game

SimpleGameWidget {
		id: gamewidget

		onGameRestarted: {
			Game.Restart()
		}

		Grid {
			property int n: 3 

			id: board
			columns: n 
			rows: n 

			property string player: "X"
			property bool pause: false

			Repeater {
				model: board.n*board.n 

				Rectangle {
					width: gamewidget.width/board.n
					height: gamewidget.height/board.n
					color: "#B0BEC5"
					id: square

					ToolButton {
						id: mousearea

						onClicked: {
							Game.Click(index);
						}

						anchors.centerIn: parent
						anchors.fill: parent
					}

					border {
						width: 1
						color: "black"
					}

					Text {
						id: text
						color: "#00000000"
					}

					Image {
						anchors.centerIn: parent
						smooth: true
						source: text.text == "X" ? "images/x.png" : "images/o.png"
						visible: text.text != "" 
					}

					property bool clickedred: false
					property bool clickedblue: false

					ColorAnimation {
						target: square 
						properties: "color"
						running: clickedred
						duration: 2500
						from: "#B0BEC5"
						to: "#F44336"
					}

					ColorAnimation {
						target: square 
						properties: "color"
						running: clickedblue
						duration: 2500
						from: "#B0BEC5"
						to: "#2196F3"
					}
				}
			}
		}
	}
