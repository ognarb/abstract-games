/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#pragma once

#include <QMainWindow>
#include <QIcon>
#include <QFile>
#include <QFileDialog>
#include <QQmlEngine>
#include <QDebug>
#include <QQuickWidget>
#include <QMessageBox>
#include <QDesktopServices>
#include <QPixmap>
#include <QMouseEvent>
#include <QQuickStyle>
#include <QCloseEvent>

#include "../closedialog/closedialog.h"

class GameWindow : public QMainWindow
{
	Q_OBJECT

public:
	GameWindow(QString name);

	void bub() { }

private:
	QQuickWidget* widget;

	QString Name;
	
protected:
	void closeEvent(QCloseEvent *event);
};
