/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "gamewindow.h"
#include <QDebug>
#include <QFont>
#include <QJSValue>
#include <QQuickItem>
#include <cstdlib>
#include <QQmlEngine>
#include <QGuiApplication>
#include <QIcon>
#include <QFile>
#include <QCursor>

GameWindow::GameWindow(QString name) : QMainWindow()
{
	Name = name;

	if (!QFile(":/main.qml").exists())
	{
		qFatal("Warning: the file :/main.qml does not exist. Gamewindow can not function properly.");
		return;
	}

	if (!QFile(":/images/icon.svg").exists())
        {
                qFatal("Warning: the file :/images/icon.svg does not exist. Gamewindow can not function properly.");
                return;
        }

	QFont font = qApp->font();
	font.setFamily("Roboto");

	QQuickStyle::setStyle("Material");
	QGuiApplication::setFont(font);

	widget = new QQuickWidget(this);
	widget->setFixedSize(600, 600);
	widget->setSource(QUrl("qrc:/main.qml"));

	setFixedSize(600, 600);
	setWindowTitle(Name);
	setWindowIcon(QIcon(":/images/icon.svg"));

	show();
	setMouseTracking(true);
}
 
void GameWindow::closeEvent(QCloseEvent *event)
{
	if (widget->rootObject()->property("closeDialog").value<QJSValue>().call().toBool()) std::exit(0);
	else event->ignore();

	/*CloseDialog* cd = new CloseDialog(this);
	cd->setWindowTitle(Name);
	cd->setWindowIcon(QIcon(":/images/icon.svg"));*/

	//if (cd->exec() == QDialog::Rejected) event->ignore();
	//else std::exit(0);
	//
}
