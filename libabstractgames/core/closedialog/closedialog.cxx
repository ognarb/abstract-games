/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "closedialog.h"
#include <QUrl>
#include <QQuickItem>

CloseDialog::CloseDialog(QWidget* parent) : QDialog(parent)
{
	QQuickWidget* widget = new QQuickWidget(this);
        widget->setFixedSize(300, 50);
        widget->setSource(QUrl("qrc:/qml/CloseDialog.qml"));

	setFixedSize(300, 50);

	connect(widget->rootObject(), SIGNAL(accepted()), this, SLOT(accept()));
	connect(widget->rootObject(), SIGNAL(rejected()), this, SLOT(reject()));
}
