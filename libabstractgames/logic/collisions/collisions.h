#include <QJSValue>
#include <QQmlEngine>
#include <QJSValue>
#include <QQuickItem>

class Collisions : public QObject {
	Q_OBJECT

public:
	Collisions();
	Q_INVOKABLE bool isColliding(QQuickItem* item1, QQuickItem* item2);
};

QJSValue collisions(QQmlEngine *engine, QJSEngine *scriptEngine);
