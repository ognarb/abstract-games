#include "simplegamewidget.h"
#include <QDebug>
#include <QUrl>
#include <QJSValue>
#include <QVariant>

SimpleGameWidgetPrivate::SimpleGameWidgetPrivate() : QQuickItem()
{
}

void SimpleGameWidgetPrivate::componentComplete()
{
	QQuickItem::componentComplete();

	QQmlEngine* engine = qmlEngine(this);
	QQmlComponent component(engine);
	component.loadUrl(QUrl("qrc:/qml/SimpleGameWidget.qml"));
	mousearea = qobject_cast<QQuickItem *>(component.create());

	mousearea->setParentItem(this);
	mousearea->setWidth(600);
	mousearea->setHeight(600);

	dialog = mousearea->findChild<QObject*>("dialog");
	dialog2 = mousearea->findChild<QObject*>("dialog2");

	connect(mousearea, SIGNAL(clicked(double,double)), this, SLOT(emitClicked(double,double)));
}

bool SimpleGameWidgetPrivate::getContainsMouse()
{
	return mousearea->property("containsMouse").toBool();
}

void SimpleGameWidgetPrivate::emitPauseGame()
{
	emit pauseGame();
}

QString SimpleGameWidgetPrivate::getDifficulty()
{
        return difficulty;
}

void SimpleGameWidgetPrivate::setDifficulty(QString _difficulty)
{
        difficulty = _difficulty;

        emit difficultyChanged(_difficulty);
}

QString SimpleGameWidgetPrivate::getPlayers()
{
        return players;
}

void SimpleGameWidgetPrivate::setPlayers(QString _players)
{
        players = _players;

        emit playersChanged(_players);
}

void SimpleGameWidgetPrivate::emitClicked(double x, double y)
{
	emit clicked(x, y);
}

void SimpleGameWidgetPrivate::restart()
{
	emit gameRestarted();
}

void SimpleGameWidgetPrivate::pause()
{
        emit gamePaused();
}

void SimpleGameWidgetPrivate::end(QString text)
{
        emit gameEnded(text);
}

void SimpleGameWidgetPrivate::new_()
{
        emit gameStarted();
}

void SimpleGameWidgetPrivate::statusWasUpdated(QString text)
{
	emit statusUpdated(text);
}

void SimpleGameWidgetPrivate::update()
{
	qApp->processEvents();
}
