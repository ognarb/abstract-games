#pragma once
#include <QQuickItem>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QMouseEvent>

class SimpleGameWidgetPrivate : public QQuickItem
{
	Q_OBJECT

	Q_PROPERTY(QString difficulty READ getDifficulty NOTIFY difficultyChanged)
	Q_PROPERTY(QString players READ getPlayers NOTIFY playersChanged)
	Q_PROPERTY(bool containsMouse READ getContainsMouse NOTIFY containsMouseChanged)

public:
        SimpleGameWidgetPrivate();

	QString getDifficulty();
	QString getPlayers();
	bool getContainsMouse();

	void restart();
	void pause();
	void end(QString text);
	void new_();
	void statusWasUpdated(QString text);
	
	Q_INVOKABLE void update();

	QObject* dialog;
	QObject* dialog2;

signals:
	void gameStarted();
	void gameRestarted();
	void endGame(QString text);
	void gameEnded(QString text);
	void updateStatus(QString text);
	void statusUpdated(QString text);
	void gamePaused();
	void pauseGame();
	void clicked(double x, double y);

	void difficultyChanged(QString difficulty);
	void playersChanged(QString players);
	void containsMouseChanged(bool containsMouse);

public slots:
	void emitClicked(double x, double y);
	void emitPauseGame();

	void setDifficulty(QString _difficulty);
	void setPlayers(QString _players);

protected:
	void componentComplete();

private:
	QString difficulty;
	QString players;
	QQuickItem* mousearea;
};
