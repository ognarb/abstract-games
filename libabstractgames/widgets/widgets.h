#include "simplegamewidget/simplegamewidget.h"
#include "gamecontroller/gamecontroller.h"

void RegisterWidgets()
{
	qmlRegisterType<GameControllerPrivate>("AbstractGames.Widgets", 1, 0, "GameControllerPrivate");
	qmlRegisterType<SimpleGameWidgetPrivate>("AbstractGames.Widgets", 1, 0, "SimpleGameWidgetPrivate");
}
