#pragma once
#include <QQuickItem>
#include <QQmlEngine>
#include <QQmlComponent>
#include "../simplegamewidget/simplegamewidget.h"

class GameControllerPrivate : public QQuickItem
{
	Q_OBJECT

	Q_PROPERTY(QString gameType READ getgameType WRITE setgameType NOTIFY gameTypeChanged)
	Q_PROPERTY(QString name READ getname WRITE setname NOTIFY nameChanged)
	Q_PROPERTY(QString difficulty READ getDifficulty NOTIFY difficultyChanged)
	Q_PROPERTY(QString players READ getPlayers NOTIFY playersChanged)

public:
        GameControllerPrivate();

	QString getgameType();
	QString getDifficulty();
	QString getPlayers();
	QString getname();

	Q_INVOKABLE bool closeDialogX();

signals:
	void gameTypeChanged(QString gameType);
	void difficultyChanged(QString difficulty);
	void playersChanged(QString players);
	void nameChanged(QString name);

private:
	QString gameType;
	QString difficulty;
	QString players;
	QString name;

	SimpleGameWidgetPrivate* gameWidget;
	QQuickItem* startupScreen;
	QQuickItem* swipeview;
	QQuickItem* thingie;

	void createGame();

public slots:
	void newGame();
	void restartGame();
	void pauseGame();
	void endGame(QString text);
	//void continueGame();
	void updateStatus(QString text);

	void setDifficulty(QString _difficulty);
	void setgameType(QString _gameType);
	void setPlayers(QString _players);
	void setname(QString _name);
};
