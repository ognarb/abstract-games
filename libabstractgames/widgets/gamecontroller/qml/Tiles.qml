/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

Rectangle
{
	Material.accent: Material.Green
	Material.theme: "Light"

	id: tiles

	width: 360
	height: 360

	property bool pause: false
	property var _menu: menu
	property string name: ""

	signal newGame()
	signal restartGame()

	function newTip()
	{
		tiprect.newTip();
	}

	MouseArea
	{
		anchors.fill: parent

		onClicked:
		{
                        if (parent.mapToGlobal(mouse.x, mouse.y).x > tip.mapToGlobal(0, 0).x &&
                                parent.mapToGlobal(mouse.x, mouse.y).y > tip.mapToGlobal(0, 0).y &&
                                parent.mapToGlobal(mouse.x, mouse.y).x < tip.mapToGlobal(tip.width, tip.height).x &&
				parent.mapToGlobal(mouse.x, mouse.y).y < tip.mapToGlobal(tip.width, tip.height).y)
			{
                                tiprect.newTip();
	                }
			else
			{
				menu.dismiss();
			}
		}
	}

	Row
	{
		anchors.top: tiles.top
		anchors.topMargin: 10
		anchors.horizontalCenter: tiles.horizontalCenter
		spacing: 6
		id: buttons

		ToolButton
		{
			font.family: "Ubuntu"
			icon.source: "images/start.svg"
			width: 50
			height: 50

			onClicked:
			{
				if (tiles.pause)
				{
					if (!menu.opened)
					{
						menu.popup();
					}
					else
					{
						menu.dismiss();
					}
				}
				else
				{
					tiles.newGame();
				}
			}

			Menu
			{
				font.family: "Ubuntu"
				id: menu
				MenuItem
				{
					text: "Resume"

					onTriggered:
					{
						tiles.newGame();
					}
				}

				MenuItem
				{
					text: "Restart"

					onTriggered:
					{
						tiles.restartGame();
					}
				}
			}
		}
	}

	Rectangle
	{
		anchors.bottom: tiles.bottom
		anchors.bottomMargin: 10
		anchors.left: tiles.left
		anchors.right: tiles.right
		color: tiles.color
		id: tiprect

		Label
		{
			Material.accent: "black"
			font.family: "Ubuntu"
			font.pointSize: 12
			horizontalAlignment: Qt.AlignHCenter
			wrapMode: Text.Wrap

			id: tip
			anchors.centerIn: parent
			text: !tiles.pause ? "<center>Click play to begin and swipe right to pause</center>" : 
				             "<center>Click play for options to restart and to continue</center>"
		}
	}
}
