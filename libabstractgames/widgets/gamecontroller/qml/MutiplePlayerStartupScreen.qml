/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

Rectangle {
        signal newGame()
	signal restartGame()
	signal displayText(string text)
	signal update()

	signal playersChanged(string text)

	property string name
    
        Material.accent: Material.Green
	Material.theme: "Light"

	signal newTip()
	onNewTip:
	{
		tiles.newTip();
	}

	MouseArea
	{
		anchors.fill: parent

		onClicked:
		{
			tiles._menu.dismiss();
		}
	}

    

		//anchors.centerIn: parent
		//anchors.fill: parent
		id: msg

		property bool spectrumonone: false 
		property bool spectrumontwo: true
       		property string text: "Click Here to Begin a Game"
		property bool pause: false

		SequentialAnimation
		{
			running: true
			loops: Animation.Infinite

			ColorAnimation
			{
				properties: "color"
				target: msg
				duration: 5000
				from: "#F44336"
				to: "#2196F3"
			}

			ColorAnimation
			{
				properties: "color"
				target: msg
				duration: 5000
				from: "#2196F3"
				to: "#4CAF50"
			}

			ColorAnimation
			{
				properties: "color"
				target: msg
				duration: 5000
				from: "#4CAF50"
				to: "#F44336"
			}
		}

		Column {
			anchors.centerIn: parent
			anchors.fill: parent

			Tiles
			{
				width: msg.width 
				height: msg.height / 2
				color: msg.color
				pause: msg.pause
				name: msg.name
				id: tiles

				onNewGame:
				{
					msg.newGame();
				}

				onRestartGame:
				{
					msg.restartGame();
				}
			}

			Rectangle
			{
				width: msg.width
				height: msg.height / 2
				color: msg.color

				SpinBox
				{
					font.family: "Ubuntu"
					anchors.bottom: parent.bottom
					anchors.bottomMargin: 10
					anchors.horizontalCenter: parent.horizontalCenter
					value: 1
					from: msg.pause ? value : 1
					to: msg.pause ? value : 2

					textFromValue: function(value)
					{
						if (value == 1) return "One Player"
						else return "Two Players"
					}

					valueFromText: function(text)
					{
						if (text == "One Player") return 1
						else return 2;
					}


					onValueModified: {
						if (displayText == "One Player")
						{
							msg.spectrumontwo = true;
							msg.playersChanged("one");
						}
						else
						{
							msg.spectrumontwo = false;
							msg.playersChanged("two");
						}
					}
				}
			}
		}
	}
