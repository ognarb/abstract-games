/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

Rectangle {
	id: msg
	signal newGame()
	signal restartGame()
	signal difficultyChanged(string text)

	property string name

	property string text: "Start Here"
	property bool easy: false
	property bool medium: true
	property bool pause: false

	signal newTip()
        onNewTip:
        {
                tiles.newTip();
        }

	MouseArea
	{
		anchors.fill: parent

		onClicked:
		{
			tiles._menu.dismiss();
		}
	}


	SequentialAnimation
                {
                        running: true
                        loops: Animation.Infinite

                        ColorAnimation
                        {
                                properties: "color"
                                target: msg
                                duration: 5000
                                from: "#F44336"
                                to: "#2196F3"
                        }

                        ColorAnimation
                        {
                                properties: "color"
                                target: msg
                                duration: 5000
                                from: "#2196F3"
                                to: "#4CAF50"
                        }

                        ColorAnimation
                        {
                                properties: "color"
                                target: msg
                                duration: 5000
                                from: "#4CAF50"
                                to: "#F44336"
                        }
                }

	Column
	{
		Tiles
		{
			width: msg.width 
			height: msg.height / 2
			color: msg.color
			pause: msg.pause
			name: msg.name
			id: tiles

			onNewGame:
			{
				msg.newGame();
			}

			onRestartGame:
			{
				msg.restartGame();
			}
		}

		Rectangle
		{
			width: msg.width
			height: msg.height / 2
			color: msg.color

			SpinBox
			{
				font.family: "Ubuntu"
				anchors.bottom: parent.bottom
				anchors.horizontalCenter: parent.horizontalCenter
				anchors.bottomMargin: 10

				value: 2
				from: msg.pause ? value : 1
				to: msg.pause ? value :  3

				textFromValue: function(value)
				{
					if (value == 1) return "Easy"
					else if (value == 2) return "Medium"
					else return "Hard"
				}

				valueFromText: function(text)
				{
					if (text == "Easy") return 1
					else if (txt == "Medium") return 2
					else return 3;
				}


				onValueModified: {
					if (displayText == "Easy")
					{
						msg.easy = true;
						msg.difficultyChanged("easy");
					}
					else if (displayText == "Medium")
					{
						msg.medium = true;
						msg.difficultyChanged("medium");
					}
					else
					{
						msg.easy = false;
						msg.medium = false;
						msg.difficultyChanged("hard");
					}
		   		 }
			}
		}
	}
}
