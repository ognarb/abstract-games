import QtQuick 2.7
import QtQuick.Controls 2.1

SwipeView {
	id:swipeview
	signal pauseGame()

	QtObject {
		id: qtobject
		property int theIndex: swipeview.currentIndex
		
		Behavior on theIndex {
			ScriptAction {
				script: {
					if (qtobject.theIndex == 1) {
						swipeview.pauseGame();
					}
				}
			}
		}
	}
}
