#include "gamecontroller.h"
//#include "../statusdialog/statusdialog.h"
#include <QDebug>
#include <QUrl>
#include <QtQml>
#include <QJSValue>
#include <QVariant>

GameControllerPrivate::GameControllerPrivate() : QQuickItem(), gameType { "" }, name { "" }
{
}

QString GameControllerPrivate::getgameType()
{
	return gameType;
}

void GameControllerPrivate::setgameType(QString _gameType)
{
	gameType = _gameType;
	emit gameTypeChanged(_gameType);

	createGame();
}

QString GameControllerPrivate::getname()
{
	return name;
}

void GameControllerPrivate::setname(QString _name)
{       
        name = _name;
        emit nameChanged(_name);
        
        createGame();
}

QString GameControllerPrivate::getDifficulty()
{
	return difficulty;
}

void GameControllerPrivate::setDifficulty(QString _difficulty)
{
	difficulty = _difficulty;

	gameWidget->setDifficulty(difficulty);
	emit difficultyChanged(_difficulty);
}

QString GameControllerPrivate::getPlayers()
{
        return players;
}

void GameControllerPrivate::setPlayers(QString _players)
{
	qDebug() << players;
        players = _players;
        
        gameWidget->setPlayers(players);
        emit playersChanged(_players);
}

void GameControllerPrivate::createGame()
{
	if (gameType != "" && name != "")
	{
		QQmlEngine* engine = qmlEngine(this);
		QQmlComponent component(engine);

		if (gameType != "None")
		{
			component.loadUrl(QUrl("qrc:/qml/SwipeView.qml"));
			swipeview = qobject_cast<QQuickItem *>(component.create());

			swipeview->setParentItem(this);
			swipeview->setWidth(600);
			swipeview->setHeight(600);
		
			if (gameType == "ArcadeGame")
			{
				component.loadUrl(QUrl("qrc:/qml/SinglePlayerStartupScreen.qml"));
			}
			else if (gameType == "BoardGame")
			{
				component.loadUrl(QUrl("qrc:/qml/MutiplePlayerStartupScreen.qml"));
			}
			else
			{
				QtQml::qmlWarning(this) << "Invaild value for property \"gameType\" - must be \"ArcadeGame\".";
			}

			startupScreen = qobject_cast<QQuickItem *>(component.create());

			startupScreen->setParentItem(swipeview);
			startupScreen->setWidth(600);
			startupScreen->setHeight(600);

			component.loadUrl(QUrl("qrc:/GameWidget.qml"));
	
			gameWidget = qobject_cast<SimpleGameWidgetPrivate *>(component.create());

			if (component.isError())
			{
				for (auto error : component.errors())
					QtQml::qmlWarning(this) << error;

				return;	
			}

			gameWidget->setParentItem(swipeview);
			gameWidget->setWidth(600);
			gameWidget->setHeight(600);

			connect(startupScreen, SIGNAL(newGame()), SLOT(newGame()));
			connect(startupScreen, SIGNAL(restartGame()), SLOT(restartGame()));

			connect(gameWidget, SIGNAL(endGame(QString)), SLOT(endGame(QString)));
			connect(gameWidget, SIGNAL(updateStatus(QString)), SLOT(updateStatus(QString)));

			connect(swipeview, SIGNAL(pauseGame()), SLOT(pauseGame()));
			connect(gameWidget, SIGNAL(pauseGame()), SLOT(pauseGame()));

			if (gameType == "ArcadeGame")
			{
				connect(startupScreen, SIGNAL(difficultyChanged(QString)), SLOT(setDifficulty(QString)));
			
				if (startupScreen->property("easy").value<bool>() == true)
				{
					difficulty = "easy";
				}
				else if (startupScreen->property("medium").value<bool>() == true)
				{
					difficulty = "medium";
				}
				else
				{
					difficulty = "hard";
				}

				gameWidget->setDifficulty(difficulty);
				emit difficultyChanged(difficulty);
			}
			else if (gameType == "BoardGame")
			{
				connect(startupScreen, SIGNAL(playersChanged(QString)), SLOT(setPlayers(QString)));

				if (startupScreen->property("spectrumontwo").value<bool>() == true)
				{
					players = "one";
				}
				else
				{
					players = "two";
				}

				gameWidget->setPlayers(players);
				emit playersChanged(players);
			}
		} else {
			component.loadUrl(QUrl("qrc:/GameWidget.qml"));

			gameWidget = qobject_cast<SimpleGameWidgetPrivate *>(component.create());

        	        if (component.isError())
                	{
                		for (auto error : component.errors())
                	        	QtQml::qmlWarning(this) << error;

				return;
                	}

               		gameWidget->setParentItem(this);
               		gameWidget->setWidth(600);
               		gameWidget->setHeight(600);

			startupScreen = nullptr;
			swipeview = nullptr;

			connect(gameWidget, SIGNAL(endGame(QString)), SLOT(endGame(QString)));
                        connect(gameWidget, SIGNAL(updateStatus(QString)), SLOT(updateStatus(QString)));

			restartGame();
		}
	}
}

void GameControllerPrivate::newGame()
{
	qDebug() << "Xj";
	gameWidget->new_();

        if (!startupScreen->property("pause").value<bool>())
        {
                gameWidget->restart();
        }

	startupScreen->setProperty("pause", false);
	swipeview->setProperty("currentIndex", 1);
}

void GameControllerPrivate::restartGame()
{
	gameWidget->restart();

	if (startupScreen && swipeview)
	{
		startupScreen->setProperty("pause", false);
		swipeview->setProperty("currentIndex", 1);
	}
}

void GameControllerPrivate::pauseGame()
{
	if (startupScreen->property("pause").value<bool>() == true) return;

	qDebug() << "XXX";

	gameWidget->pause();

	startupScreen->setProperty("pause", true);
	swipeview->setProperty("currentIndex", 0);
}

void GameControllerPrivate::endGame(QString text)
{
	if (startupScreen)
	{
		if (startupScreen->property("pause").value<bool>()) return;
	}

	/*StatusDialog* statusDialog = new StatusDialog(text);
	statusDialog->setWindowTitle(name);
        statusDialog->setWindowIcon(QIcon(":/images/icon.svg"));
        statusDialog->exec();*/

	gameWidget->dialog->setProperty("text", text);
        gameWidget->dialog->setProperty("visible", true);

	gameWidget->end(text);

	if (startupScreen && swipeview)
	{
		swipeview->setProperty("currentIndex", 0);
		startupScreen->setProperty("pause", false);
	}
	else
	{
		restartGame();
	}
}

void GameControllerPrivate::updateStatus(QString text)
{
	if (startupScreen)
	{
		if (startupScreen->property("pause").value<bool>()) return;
	}

	/*StatusDialog* statusDialog = new StatusDialog(text);
	statusDialog->setWindowTitle(name);
	statusDialog->setWindowIcon(QIcon(":/images/icon.svg"));
	statusDialog->exec();*/

	gameWidget->dialog->setProperty("text", text);
	gameWidget->dialog->setProperty("visible", true);

	while (true)
        {
                qApp->processEvents();

                if (gameWidget->dialog->property("visible") == false)
                {
			qDebug() << "X";
			return;
                }
        }

	gameWidget->statusWasUpdated(text);
}

bool GameControllerPrivate::closeDialogX()
{
	gameWidget->pause();

	if (startupScreen)
	{
		if (swipeview->property("currentIndex").value<int>() == 0) return true;
	}

	gameWidget->dialog2->setProperty("text", "You're sure?");
	gameWidget->dialog2->setProperty("visible", true);

	while (true)
	{
		qApp->processEvents();

		if (gameWidget->dialog2->property("visible") == false)
		{
			if (gameWidget->dialog2->property("result").value<int>() == 1) return true;
			else
			{
				gameWidget->new_();
				return false;
			}
		}
	}
}
