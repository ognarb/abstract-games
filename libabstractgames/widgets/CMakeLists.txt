cmake_minimum_required(VERSION 3.5)
project ("Lib Abstract Games Widgets")

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)

set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOMOC ON)

find_package(Qt5 COMPONENTS Widgets QuickWidgets QuickControls2 REQUIRED)
include_directories(${CMAKE_BINARY_DIR})
add_definitions(${QT_DEFINITIONS})

set(ABSTRACTGAMESWIDGETS_SRCS gamecontroller/gamecontroller.cxx
	simplegamewidget/simplegamewidget.cxx)
	#statusdialog/statusdialog.cxx)
set(ABSTRACTGAMESWIDGETS_QRCS gamecontroller/gamecontroller.qrc
	gamecontroller/gamecontroller.qrc
	simplegamewidget/simplegamewidget.qrc
	#statusdialog/statusdialog.qrc
	simplegamewidget/simplegamewidget.qrc)

set(LIBS Qt5::Widgets Qt5::QuickWidgets Qt5::QuickControls2)

set(ABSTRACTGAMESWIDGETS_TAR abstractgames.widgets)

add_library(${ABSTRACTGAMESWIDGETS_TAR} SHARED ${ABSTRACTGAMESWIDGETS_SRCS} ${ABSTRACTGAMESWIDGETS_QRCS})
target_link_libraries(${ABSTRACTGAMESWIDGETS_TAR} PRIVATE ${LIBS})

install(TARGETS abstractgames.widgets DESTINATION "/usr/lib")
