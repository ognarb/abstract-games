# Abstract Games

Abstract Games is the games suite for The Abstract Software Project. It’s contents are:

* Chess - a chess program with a AI to play against

* SameGame - a same-game program

* Mines - a mines program

* TicTacToe - a tic-tac-toe program with a AI to play against

* Bots - a robots game

* JetFighter - the classic Bomber game

* iGo - the board game go

You can play against the computer, and use touchpad gestures. For more infomation, see [The Abstract Game's Wiki](https://gitlab.com/abstractsoftware/abstract-games/-/wikis/home).

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/abstractgames)

This software is licensed under the Mozilla Public License. See the file `LICENSE` in the root directory for more infomation.

Most of the source code and images I did not make - see the file `CREDITS` for all of the credits.

The TODO list for Abstract Games is in the file `TODO`.

Thank you for using Abstract Games, The Abstract Software Team (A&M)
