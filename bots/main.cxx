/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <QApplication>
#include <QQuickView>
#include <QQmlEngine>
#include <QUrl>
#include <QIcon>

#include "../libabstractgames/core/gamewindow/gamewindow.h"
#include "../libabstractgames/widgets/gamecontroller/gamecontroller.h"
#include "../libabstractgames/widgets/simplegamewidget/simplegamewidget.h"
#include "../libabstractgames/logic/collisions/collisions.h"

int main(int argc, char* argv[])
{
        qmlRegisterType<GameControllerPrivate>("AbstractGames.Widgets", 1, 0, "GameControllerPrivate");
        qmlRegisterType<SimpleGameWidgetPrivate>("AbstractGames.Widgets", 1, 0, "SimpleGameWidgetPrivate");
	qmlRegisterSingletonType("AbstractGames.Logic", 1, 0, "Collisions", collisions);

        QApplication* app = new QApplication(argc, argv);
        GameWindow* win = new GameWindow("Bots");

        return app->exec();
}
