/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

/*
	Core game functions.
*/

function Restart()
{
	/*frog.x = (gamewidget.width / 2) - frog.width;
	frog.y = 10;

	for (var i = 0; i < cars.children.length; i++)
	{
		if (i == 6) break;
		cars.children[i].speed = (Math.random() * 25) + 10;
		cars.children[i].timer.running = true;
		cars.children[i].timer.start();
	}*/

	gamewidget.level = 1;
	gamewidget.lifes = 1;

	gotoNextLevel();
}

function movefrog(dir)
{
	movingfrog.dir = dir;
	movingfrog.running = true;
}

function movefrogbyone(dir)
{
	if (dir == "north")
		frog.y -= 5;
	else if (dir == "south")
		frog.y += 5;
	else if (dir == "east")
		frog.x -= 5;
	else if (dir == "west")
		frog.x += 5;

	if (Math.abs((frog.y - 0.1*end.height) - end.y) < 10)
	{
		frog.begin         = false;
		timer.running      = false;
		movingfrog.running = false;
		movingfrog.stop();
		timer.stop();

		for (var i = 0; i < gamewidget.num; i++)
		{
			console.log("X");
			cars.children[i].running = false;
			cars.children[i].timer.stop();
		}

		var all = true;

		for (var i = 0; i < 3; i++) { if (tokens.children[i].visible) all = false; }

		if (all)
		{
			if (gamewidget.level != gamewidget.maxlevels)
			{
				gamewidget.level += 1;
				gamewidget.updateStatus("Onto level " + gamewidget.level + ".");
				gotoNextLevel();
			}
			else
			{
				gamewidget.endGame("You won!");
				return;
			}
		}
		else
		{
			gamewidget.updateStatus("There're more hearts to get!");

			//for (var iui = 0; iui < 10000; iui++) gamewidget.update();;

		}

		for (var i = 0; i < gamewidget.num; i++)
                {
                        console.log("X");
                        cars.children[i].timer.start();
		}

		timer.start();

		return;
	}

	if (frog.x < 0) frog.x = 0;
	if (frog.x > gamewidget.width) frog.x = gamewidget.width - frog.width;
}

function gotoNextLevel()
{
	frog.x = (gamewidget.width / 2) - frog.width;
	frog.y = 10;

	islands.children[0].x = 100;
	islands.children[1].x = 300;

	for (var i = 0; i < cars.children.length; i++)
	{
		if (i == gamewidget.num) break;
		cars.children[i].speed = (Math.random() * gamewidget.speed) + 10;
	}

	for (var i = 0; i < gamewidget.num; i++) { cars.children[i].running = true; cars.children[i].timer.start();  }

	for (var i = 0; i < 3; i++)
        {
                tokens.children[i].visible = true;
                tokens.children[i].x       = Math.random * (600 - tokens.children[i].width);
        }

	timer.running = true;

	gamewidget.lifes = 1;
}

function movecar(index, interval = -1)
{
	var speed = interval;
	if (interval == -1) speed = cars.children[index].speed;

	console.log(index, index % 2);
                                        cars.children[index].x += cars.children[index].dir*speed;

                                        if (cars.children[index].dir == +1)
                                        {
                                                if (cars.children[index].x > gamewidget.width)
                                                {
                                                        cars.children[index].x = 0;
                                                }
                                        }
                                        else
                                        {
                                                if (cars.children[index].x < 0)
                                                {
                                                        cars.children[index].x = gamewidget.width
                                                }
                                        }
}
