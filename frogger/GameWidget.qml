import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Controls.Material 2.3
import "game.js" as Game
import AbstractGames.Logic 1.0 as Logic

SimpleGameWidget {
	id: gamewidget

	property int level: 1
	property int maxlevels: 1
	property int speed: level == 1 ? 15 : level == 2 ? 25 : 35
	property int num: level == 1 ? 3 : 6
	property int lifes: 0

	onGameRestarted: {
		Game.Restart();
	}

	onGameEnded: {
		/*timer.running = false;

		for (var i = 0; i < 3; i++)
		{
			cars.children[i].running = false;
			cars.children[i].timer.stop();
		}

		frog.begin = false;
                                                timer.running = false;
                                                movingfrog.running = false;
                                                timer.stop();
						movingfrog.stop();*/
	}

	onGamePaused: {
		timer.running = false;

		for (var i = 0; i < num; i++) cars.children[i].running = false;

	}

	onGameStarted: {
		timer.running = true;

		for (var i = 0; i < num; i++) cars.children[i].running = true;
	}

	Text {
		visible: false
                anchors.left: parent.left
		anchors.leftMargin: 10
		anchors.top: parent.top
		anchors.topMargin: 10
                color: Material.color(Material.Red)
		text: "lives = " + gamewidget.lifes
		z: 100
		font.family: "Ubuntu"
		font.pointSize: 12
        }


		Timer {
			id: timer
			interval: 1
			repeat: true

			onTriggered: {
				for (var i = 0; i < cars.children.length; i++)
				{
					if (Logic.Collisions.isColliding(frog, cars.children[i]))
					{
						for (var ik = 0; ik < 10000; ik++) ;
						console.log("X");

						for (var io = 0; io < cars.children.length; io++)
						{
							if (io == num) break;
							cars.children[io].timer.running = false;
							cars.children[io].timer.stop();
						}
						frog.begin = false;
						timer.running = false;
						movingfrog.running = false;
						timer.stop();
						movingfrog.stop();
						gamewidget.lifes--;

						if (gamewidget.lifes == 0)
						{
							gamewidget.updateStatus("You lose");
							Game.gotoNextLevel();
							//return;
						}
						else
						{
							//gamewidget.updateStatus("You have " + gamewidget.lifes + " lives");
							//timer.running = true;
						}

						Game.movecar(i, cars.children[i].width + frog.width);

						for (var i = 0; i < cars.children.length; i++)
                                                {
                                                        if (i == num) break;
                                                        cars.children[i].timer.running = true;
                                                        cars.children[i].timer.start();
						}

						timer.start();
						//movingfrog.stop();
						//return;
					}
				}

				for (var i = 0; i < 3; i++)
				{
					if (tokens.children[i].visible)
					{
						if (Logic.Collisions.isColliding(frog, tokens.children[i]))
						{
							tokens.children[i].visible = false;
							gamewidget.lifes++;
					       }
					}
				}

			       if (Qt.application.state == Qt.ApplicationInActive)
                        {
                                console.log("X");
                                gamewidget.pauseGame();
                        }
			}
		}

		Timer {
			id: movingfrog
			interval: 1
			repeat: true
			property string dir: ""

			onTriggered: {
				if (!frog.begin)
				{
					running = false;
					stop();
					return;
				}
				Game.movefrogbyone(dir);
			}
		}

		Image {
			id: frog
			width: 24
			property bool begin: false
			height: 24
			source: "images/frog.svg"
			x: gamewidget.width / 2
			y: 10
			z: 10

			property int oldx: 0
			property int oldy: 0
		}

		Rectangle {
			id: start
			anchors.left: parent.left
			anchors.right: parent.right
			height: 40
			y: 0
			color: "black"
		}

		Rectangle {
			color: "transparent"
			anchors.fill: parent
			id: islands

			Repeater {
				model: 2
				
				Rectangle {
					radius: 5
					id: x
					height: 40
					width: 200
					y: (index * 180) + 180
					color: "black"
				}
			}
		}

		// index * 180 + 90

		Rectangle {
			color: "transparent"
			anchors.fill: parent
			id: tokens

			Repeater {
				model: 3

				Image {
					width: 25
					height: 25
					source: "images/token.svg"
					x: Math.random() * (600 - width)
					y: index * 180 + 97
				}
			}
		}

	Rectangle {
	color: "transparent"
	anchors.fill: parent
	id: cars

	Repeater {
		model: num 

		Image {
			property bool begin: false
			property int speed: (Math.random() * 800) + 500
			property bool running: true
			property var timer: t
			z: 10
			width: 80
			height: 30
			//color: "red"
			source: (index % 2) == 0 ? "images/car-left.svg" : "images/car-right.svg"
			//text: index
			x: (index % 2) == 0 ? 0 : gamewidget.width
			property int dir: (index % 2) == 0 ? +1 : -1
			y: (Math.floor(num == 6 ? index * 0.5 : index) * 180) + 10 + 
				(num == 6 ? ((index % 2) == 0 ? 50 : 120) : 80)

			Timer {
				id: t
				repeat: true
				interval: 100
				running: parent.running

				onTriggered: {
					Game.movecar(index);
				}
			}
		}	
	}
}

		Rectangle {
			id: end
			anchors.left: parent.left
                        anchors.right: parent.right
                        height: 40
			y: gamewidget.height - 40
			color: "black"
		}


		Row
		{
			parent: start
			anchors.fill: parent
			spacing: 10
			leftPadding: 10
			//topPadding: 2.5
			//x: 10
			//y: 10


			Text
			{
				text: "<"
				color: "white"
				font.family: "Ubuntu"
				font.pixelSize: 25
				z: 500
				anchors.verticalCenter: parent.verticalCenter
	

				MouseArea
				{
					anchors.fill: parent

					onPressed:
					{
						frog.begin = true;
						Game.movefrog("east");
					}

					onReleased:
					{
						frog.begin = false;
					}
				}
			}

			Text
			{
				text: ">"
				font.family: "Ubuntu"
				font.pixelSize: 25
				color: "white"
				z: 500
				 anchors.verticalCenter: parent.verticalCenter
                   
		                    MouseArea
				{
					anchors.fill: parent

					onPressed:
                                               {
                                                       frog.begin = true;
                                                       Game.movefrog("west");
                                               }

                                               onReleased:
                                               {
                                                       frog.begin = false;
                                               }
				}
			}

			Text
			{
				color: "white"
				text: "<"
				font.family: "Ubuntu"
				rotation: 90
				font.pixelSize: 25
				z: 500
				 anchors.verticalCenter: parent.verticalCenter
                   
		                    MouseArea
				{
					anchors.fill: parent
					onPressed:
                                               {
                                                       frog.begin = true;
                                                       Game.movefrog("north");
                                               }

                                               onReleased:
                                               {
                                                       frog.begin = false;
                                               }
				}
			}

			Text
			{
				text: ">"
				font.family: "Ubuntu"
				rotation: 90
				font.pixelSize: 25
				color: "white"
				z: 500
				 anchors.verticalCenter: parent.verticalCenter
                   
		                    MouseArea
				{
					anchors.fill: parent

					onPressed:
                                               {
                                                       frog.begin = true;
                                                       Game.movefrog("south");
                                               }

                                               onReleased:
                                               {
                                                       frog.begin = false;
                                               }
				}
			}

			Text
			{
				text: "lives left: " + gamewidget.lifes
				font.family: "Ubuntu"
				font.pixelSize: 15
				color: "white"
				z: 5000
				anchors.verticalCenter: parent.verticalCenter
			}
		}
	}
